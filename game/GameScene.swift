//
//  GameScene.swift
//  game
//
//  Created by Jane Idelson on 13/02/19.
//  Copyright © 2019 NoName. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    private var bird = SKSpriteNode()
    private var background = SKSpriteNode()
    private var timer = Timer()
    
    private var gameOver = false
    
    private enum CollaiderType: UInt32 {
        case bird = 0
        case object
        case gap
    }
    
    override func didMove(to view: SKView) {
        createBird()
        createBackground()
        
        // after pipes method!
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(creaetPipes), userInfo: nil, repeats: true)
        
        // collision detection
        physicsWorld.contactDelegate = self
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !gameOver {
            bird.physicsBody?.isDynamic = true
            bird.physicsBody?.velocity = CGVector(dx: 0, dy: 0) // slows bird down
            bird.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 150))
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        let passGap = contact.bodyA.contactTestBitMask == CollaiderType.gap.rawValue || contact.bodyB.contactTestBitMask == CollaiderType.gap.rawValue
        if passGap {
            //score += 1
        } else {
            // first step = no if/else checking!
            print("😅")
            speed = 0 // stop the game when collusion is detected
            gameOver = true
        }
    }
    
    private func createBird() {
        let birdTexture1 = SKTexture(imageNamed: "flappy1")
        let birdTexture2 = SKTexture(imageNamed: "flappy2")
        
        let animation = SKAction.animate(with: [birdTexture1, birdTexture2], timePerFrame: 0.1)
        let repeatFlappyingForever = SKAction.repeatForever(animation)
        
        bird = SKSpriteNode(texture: birdTexture1)
        bird.position = CGPoint(x: frame.midX, y: frame.midY)
        
        bird.run(repeatFlappyingForever)
        addChild(bird)
        
        bird.physicsBody = SKPhysicsBody(circleOfRadius: birdTexture1.size().width / 2)
        bird.physicsBody?.isDynamic = false
        
        // Defines what logical 'categories' of bodies this body generates intersection notifications with.
        bird.physicsBody!.contactTestBitMask = CollaiderType.object.rawValue
        // Defines what logical 'categories' this body belongs to.
        bird.physicsBody!.categoryBitMask = CollaiderType.bird.rawValue
        // Defines what logical 'categories' of bodies this body responds to collisions with.
        bird.physicsBody!.collisionBitMask = CollaiderType.bird.rawValue // not really need to set it
    }
    
    private func createBackground() {
        let backgroundTexture = SKTexture(imageNamed: "bg")
        let bgMovement = SKAction.move(by: CGVector(dx: -backgroundTexture.size().width, dy: 0), duration: 7)
        let bgMoveBack = SKAction.move(by: CGVector(dx: backgroundTexture.size().width, dy: 0), duration: 0)
        let moveBGForever = SKAction.repeatForever(SKAction.sequence([bgMovement, bgMoveBack]))
        
        var counter: CGFloat = 0
        while counter < 3 {
            background = SKSpriteNode(texture: backgroundTexture)
            background.position = CGPoint(x: backgroundTexture.size().width * counter, y: frame.midY)
            background.size.height = frame.height
            background.run(moveBGForever)
            addChild(background)
            background.zPosition = -1
            counter += 1
        }
        
        let ground = SKNode()
        
        ground.position = CGPoint(x: frame.midX, y: -frame.height / 2)
        ground.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: frame.width, height: 1))
        ground.physicsBody?.isDynamic = false
        
        // once we set the physics body the bird is now able to move through the ground
        
        ground.physicsBody!.contactTestBitMask = CollaiderType.object.rawValue
        ground.physicsBody!.categoryBitMask = CollaiderType.object.rawValue
        ground.physicsBody!.collisionBitMask = CollaiderType.object.rawValue
        addChild(ground)
    }
    
    @objc private func creaetPipes() {
        let topPipeTexture = SKTexture(imageNamed: "pipe1")
        let bottomPipeTexture = SKTexture(imageNamed: "pipe2")
        
        let topPipe = SKSpriteNode(texture: topPipeTexture)
        let bottomPipe = SKSpriteNode(texture: bottomPipeTexture)
        
        let gapHeight = bird.size.height * 3 // setting this will make the game easier or harder
        let movementAmount = Int.random(in: 0..<Int((frame.size.height / 2)))
        let pipeOffset = CGFloat(movementAmount - Int(frame.size.height / 4)) // +/- quarter of the screen
        
        let movePipes = SKAction.move(by: CGVector(dx: -2 * frame.size.width, dy: 0), duration: TimeInterval(frame.size.width / 100))
        
        topPipe.position = CGPoint(x: frame.midX + frame.width, y: frame.midY + topPipeTexture.size().height / 2 + gapHeight + pipeOffset)
        bottomPipe.position = CGPoint(x: frame.midX + frame.width, y: frame.midY - bottomPipeTexture.size().height / 2 - gapHeight + pipeOffset)
        
        topPipe.run(movePipes)
        bottomPipe.run(movePipes)
        
        // setting contact bitmask
        topPipe.physicsBody = SKPhysicsBody(rectangleOf: topPipeTexture.size())
        topPipe.physicsBody?.isDynamic = false
        
        topPipe.physicsBody!.contactTestBitMask = CollaiderType.object.rawValue
        topPipe.physicsBody!.categoryBitMask = CollaiderType.object.rawValue
        topPipe.physicsBody!.collisionBitMask = CollaiderType.object.rawValue
        
        bottomPipe.physicsBody = SKPhysicsBody(rectangleOf: bottomPipeTexture.size())
        bottomPipe.physicsBody?.isDynamic = false
        
        bottomPipe.physicsBody!.contactTestBitMask = CollaiderType.object.rawValue
        bottomPipe.physicsBody!.categoryBitMask = CollaiderType.object.rawValue
        bottomPipe.physicsBody!.collisionBitMask = CollaiderType.object.rawValue
        
        // create the gap so we could score the game
        
        let gap = SKNode()
        gap.position = CGPoint(x: frame.midX + frame.width, y: frame.midY + pipeOffset) // center + pipe offset
        gap.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: bottomPipeTexture.size().width, height: gapHeight * 2))
        gap.physicsBody?.isDynamic = false
        
        gap.physicsBody!.contactTestBitMask = CollaiderType.bird.rawValue
        gap.physicsBody!.categoryBitMask = CollaiderType.gap.rawValue
        gap.physicsBody!.collisionBitMask = CollaiderType.gap.rawValue
        
        gap.run(movePipes)
        
        addChild(gap)
        addChild(topPipe)
        addChild(bottomPipe)
    }
}
